package com.atguigu.springcloud.service;

import ch.qos.logback.core.util.TimeUtil;
import cn.hutool.core.util.IdUtil;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.concurrent.TimeUnit;

@Service
public class PaymentService {
    //正常访问
    public String paymentInfo_OK(Integer id){
        return "线程池"+Thread.currentThread().getName() + "  paymentInfo_OK,id:" + id + "\t" + "=-=";

    }
    //访问超时
    @HystrixCommand(fallbackMethod = "paymentInfo_TimeOutHandler",commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "5000")
    })
    public String paymentInfo_TimeOut(Integer id){
        try{
            TimeUnit.MICROSECONDS.sleep(3000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        int i= 10/0;
        return "线程池"+Thread.currentThread().getName() + "  paymentInfo_OK,id:" + id + "\t" + "=-=" +"耗时三秒";

    }

    public String paymentInfo_TimeOutHandler(Integer id){
        return "兜底方法";
    }


    @HystrixCommand(fallbackMethod = "paymentCircuitBreaker_dallback",commandProperties = {
            @HystrixProperty(name="circuitBreaker.enabled" , value = "true"),//是否开启断路器
            @HystrixProperty(name="circuitBreaker.requestVolumeThreshold" , value = "10"),//请求次数
            @HystrixProperty(name="circuitBreaker.sleepWindowInMilliseconds" , value = "10000"),//时间窗口期
            @HystrixProperty(name="circuitBreaker.errorThresholdPercentage" , value = "60")//失败率达到多少
    })
    //服务熔断
    public String paymengtCircuitBrraker(@PathVariable("id") Integer id){
        if (id < 0){
            throw new RuntimeException("******id 不能是负数");
        }
        String SerialNumber = IdUtil.simpleUUID();
        return Thread.currentThread().getName() + "调用成功" + SerialNumber;
    }

    public String paymentCircuitBreaker_dallback(@PathVariable("id") Integer id){
        return "id 不能是负数，稍后再试";
    }
}
