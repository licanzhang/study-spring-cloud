package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.entities.CommonResult;
import com.atguigu.springcloud.entities.Payment;
import com.atguigu.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

@RestController
@Slf4j
@RequestMapping("/Payment")
public class PaymentController {
    @Resource
    private PaymentService paymentService;
    @PostMapping(value = "/create")
    public CommonResult create(@RequestBody Payment payment) {
        int i = paymentService.create(payment);
        log.info("结果" + i);
        if (i>0) {
            return new CommonResult(200,"添加成功",i);
        }else {
            return new CommonResult(444,"添加失败");
        }
    }
    @GetMapping(value = "/get/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id){
        Payment payment = paymentService.getPaymentById(id);
        
        log.info("结果:" + payment + payment);
        if (payment!=null) {
            return new CommonResult(200,"成功" ,payment);
        }else {
            return new CommonResult(444,"没有记录" + id);
        }
    }
        @GetMapping("/Feign/timeout")
    public String PaymentFeignTimeOut(){
        try{
            TimeUnit.SECONDS.sleep(3);
        }catch (InterruptedException e) {
            e.printStackTrace();
        }
        return "404";
    }
}
