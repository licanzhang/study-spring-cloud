package com.atguigu.springcloud.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
public class PaymentFallbackService  implements PaymentHystrixService{
    @Override
    public String paymentInfo_OK(Integer id) {
        return "实现类中的OK方法";
    }

    @Override
    public String paymentInfo_TimeOut(Integer id) {
        return "实现类中的timeout方法";
    }
}
